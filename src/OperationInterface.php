<?php

namespace PagePicnic;

interface OperationInterface {
	public function run($num, $current);
}