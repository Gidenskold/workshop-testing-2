<?php

namespace PagePicnic\Operations;

use PagePicnic\OperationInterface;

class Addition implements OperationInterface {
	public function run($num, $current) {
		return $current + $num;
	}
}