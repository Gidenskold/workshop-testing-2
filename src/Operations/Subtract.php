<?php

namespace PagePicnic\Operations;

use PagePicnic\OperationInterface;

class Subtract implements OperationInterface {
	public function run($num, $current) {
		return $current - $num;
	}
}