<?php 

namespace PagePicnic;

class Calculator {
	protected $result = 0;
	protected $operands = array();
	protected $operation;

	public function setOperands()
	{
	  $this->operands = func_get_args();
	}

	public function setOperation(OperationInterface $operation)
	{
	  $this->operation = $operation;
	}

	public function calculate()
	{
	  foreach ($this->operands as $num) {
	    if (!is_numeric($num)) {
	      throw new \RuntimeException("Numbers plz");
	    }

	    $this->result = $this->operation->run($num, $this->result);
	  }

	  return $this->result;
	}
}