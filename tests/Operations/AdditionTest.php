<?php

namespace PagePicnic\Operations;

use PHPUnit_Framework_TestCase;

class AdditionTest extends PHPUnit_Framework_TestCase {
	public function test_that_it_adds_the_numbers() {
		$addition = new Addition;
		$sum = $addition->run(5, 6);
		$this->assertEquals(11, $sum, "The Addition class should add");
	}
}