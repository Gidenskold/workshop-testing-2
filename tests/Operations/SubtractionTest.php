<?php

namespace PagePicnic\Operations;

use PHPUnit_Framework_TestCase;

class SubtractionTest extends PHPUnit_Framework_TestCase {
	public function test_that_it_subtracts_the_numbers() {
		$subtraction = new Subtract;
		$sum = $subtraction->run(2, 6);
		$this->assertEquals(4, $sum, "The Subtraction class should add");
	}
}