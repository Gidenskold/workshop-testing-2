<?php

namespace PagePicnic;

use PHPUnit_Framework_TestCase;
use PagePicnic\Operations\Addition;
use PagePicnic\Operations\Subtract;
use Mockery;

class CalculatorTest extends PHPUnit_Framework_TestCase {
	protected $calc;

	public function setUp() {
		$this->calc = new Calculator;
	}

	/**
	 * @expectedException \RuntimeException
	 */
	public function test_that_it_requires_a_numeric_value() {
		$this->calc->setOperands("foo");
		$this->calc->setOperation(new Addition);
		$this->calc->calculate();
	}

	public function test_that_is_accepts_multiple_arguments() {
		$operation = Mockery::mock("PagePicnic\\OperationInterface");

		$operation
			->shouldReceive("run")
			->with(1, 0)
			->andReturn(100);

		$operation
			->shouldReceive("run")
			->with(2, 100)
			->andReturn(666);
		
		$this->calc->setOperands(1, 2);
		$this->calc->setOperation($operation);
		$this->assertEquals(666, $this->calc->calculate());
	}

	public function test_that_it_calculates_with_given_operation() {
		$operation = Mockery::mock("PagePicnic\\OperationInterface");

		$operation
			->shouldReceive("run")
			->with(5, 0)
			->andReturn(999);
		
		$this->calc->setOperands(5);
		$this->calc->setOperation($operation);
		$this->assertEquals(999, $this->calc->calculate());
	}
}